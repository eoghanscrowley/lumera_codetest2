import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as actionCreators from '../../actions/postsActionCreators'

class LandingPage extends Component {
  render() {
    return (
      <div>Landing Page</div>
    )
  }
}

// the functions below are needed in order to configure how state
// is connected to this component using react-redux
function mapStateToProps(state) {
  return { ...state.posts }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage)
