import { combineReducers } from 'redux'

import posts from './posts'

// this combines any reducers you want to be used in creating the store
const rootReducer = combineReducers({
  posts,
})

export default rootReducer
