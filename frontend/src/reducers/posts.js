const defaultState = []

// this is an example reducer.
// its basically just a switch statement which returns a
// new version of state which depends on the action type which is fed in
export default function posts(state = defaultState, action) {
  switch (action.type) {
    default:
      return state
  }
}
