import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import promiseMiddleware from 'redux-promise-middleware'
import createHistory from 'history/createBrowserHistory'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from './reducers/index'

const history = createHistory()
const routerStuff = routerMiddleware(history)

export { history }

// this adds promise and thunk middleware, these are 2 options for
// helping writing asynchronous requests
const middleware = applyMiddleware(routerStuff, promiseMiddleware(), thunk)

// this will combine middleware with redux devtools depending on if the environment is development
const enhancers =
  process.env.NODE_ENV === 'development' ?
    composeWithDevTools(middleware)
    : middleware

// this creates the store with the enhancers
const store = createStore(rootReducer, {}, enhancers)

export default store
